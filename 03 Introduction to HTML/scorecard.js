var maxScorePicker = null;
var pointOne = null;
var pointTwo = null;
var reset = null;

function getElements(){
	maxScorePicker = document.querySelector("#maxScorePicker");
	pointOne = document.querySelector("#addPointOne");
	pointTwo = document.querySelector("#addPointTwo");
	reset = document.querySelector("#reset");
};

getElements();
pointOne.addEventListener("click", updateScore);
pointTwo.addEventListener("click", updateScore);
reset.addEventListener("click", resetGame);

function updateScore(){
	if(	pointOne.disabled ||
		pointTwo.disabled){
		return;
	}

	maxScorePicker.disabled = true;

	var playerId = this.value.substring("Player ".length);
	var playerScoreElement = document.querySelector("#"+playerId);
	var playerScore = playerScoreElement.innerText*1 + 1;
	playerScoreElement.innerText = playerScore;

	if(playerScore == maxScorePicker.value){
		playerScoreElement.classList.add("winner");
		pointOne.disabled=true;
		pointTwo.disabled=true;
	}
};

function resetGame(){
	resetScores();
	enableButtons();
};

function resetScores(){
	var scoreOne = document.querySelector("#One");
	var scoreTwo = document.querySelector("#Two");
	scoreOne.classList.remove("winner");
	scoreTwo.classList.remove("winner");
	scoreOne.innerText = "0";
	scoreTwo.innerText = "0";
};

function enableButtons(){
	pointOne.disabled=false;
	pointTwo.disabled=false;
	maxScorePicker.disabled = false;
};