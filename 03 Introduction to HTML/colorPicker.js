var colorBlockList = document.querySelectorAll(".square");
var rgbInfo = document.querySelector("#rgbInfo");
var header = document.querySelector(".heade");
var nav = document.querySelector("#nav");
var msg = document.querySelector("#message");
var diff = document.querySelector("#difficulty");
var easy = document.querySelector("#diffEasy");
var hard = document.querySelector("#diffHard");
var newColorsBtn = document.querySelector("#newColors");

var gameOver = false;
var diffHard = true;
var colorArray = [];
var numTiles = 6;
var maxArrLen = numTiles * 3; //rgb values for numTiles # of colors

populateColors();

newColorsBtn.addEventListener("click", populateColors);
diff.addEventListener("click",toggleDifficulty);

function toggleDifficulty(){

	if(diffHard){
		diffHard = false;
		numTiles = 3;
		easy.classList.add("chosenDifficulty");
		hard.classList.remove("chosenDifficulty");
	}else{
		diffHard = true;
		numTiles = 6;
		easy.classList.remove("chosenDifficulty");
		hard.classList.add("chosenDifficulty");
	}
	maxArrLen = numTiles * 3; //rgb values for numTiles # of colors

	populateColors();
};

function checkIfTileCorrect(){
	if(!gameOver){
		showObj(msg);
		if(this.classList.contains("correct")){
			colorBlockList.forEach(function(tile){
				showObj(tile);
				tile.style.backgroundColor = rgbInfo.textContent;
			});
			header.classList.remove("heade");
			header.classList.add('correctColor');
			//element is null & throws JS error if click correct tile a 2nd time -> gameOver bool
			if(diffHard){
				hard.classList.add("correctColor");
				hard.classList.remove("chosenDifficulty");
			}else{
				easy.classList.add("correctColor");
				easy.classList.remove("chosenDifficulty");
			}
			createCorrectCss(rgbInfo.textContent);
			msg.textContent = "Correct!";
			gameOver = true;
			toggleNewBtn();
			return;
		}
		hideObj(this);
		msg.textContent = "Try Again";
	}
};

function populateColors(){
	resetField();
	console.log(colorArray);
	var correct = Math.floor(Math.random() * numTiles);
	while(colorArray.length > 0){
		var colorBlockIndex = (numTiles)-(colorArray.length/3);
		var numArr = [numTiles, colorArray.length, colorBlockIndex];
		console.log(numArr);
		var r = Number(colorArray.pop());
		var g = Number(colorArray.pop());
		var b = Number(colorArray.pop());

		var colorString = formatToRgb([r,g,b]);
		if(colorBlockIndex == correct){
			rgbInfo.textContent = colorString;
			colorBlockList[colorBlockIndex].classList.add("correct");
		}
		colorBlockList[colorBlockIndex]
			.style.backgroundColor = colorString;
	}


	colorBlockList.forEach(function(tile){
		tile.addEventListener("click",checkIfTileCorrect);
	});
};

function createCorrectCss(rgbColor){
	var style = document.createElement('style');
	style.type = 'text/css';
	style.innerHTML = '.correctColor { background-color: '+rgbColor+'; }';
	document.getElementsByTagName('head')[0].appendChild(style);
	
	document.getElementById('heade').className = 'correctColor';
};

function formatToRgb(rgbArray){
	return "rgb(" +
			rgbArray.join(", ")
			+ ")";
}

function showObj(o){
	o.classList.remove("hide");
	o.classList.add("show");
}

function hideObj(o){
	o.classList.add("hide");
	o.classList.remove("show");
}

function resetField(){
	gameOver = false;
	toggleNewBtn();
	clearDivSquares();
	createAllDivSquares();
	resetMessage();
	resetDiffCss()
	resetHeader();
	populateNewColorArray();
	resetTiles();
	cleanCssChildren();
};

function toggleNewBtn(){
	if(gameOver){
		newColorsBtn.textContent="Play Again";
	}else{
		newColorsBtn.textContent="New Colors";
	}
}

function clearDivSquares(){
	var con = document.getElementById("container")
	while(con.firstChild){
		con.removeChild(con.firstChild);
	}
}

function createAllDivSquares(){
	for(var i=0;i<numTiles;i++){
		createSingleDivSquare();
	}
	colorBlockList = document.querySelectorAll(".square");
}

function createSingleDivSquare(){
	var div = document.createElement('div');
	div.classList.add("square");
	document.getElementById('container').appendChild(div);
};

function resetMessage(){
	hideObj(msg);
}

function resetDiffCss(){
	if(diffHard){
		hard.classList.add("chosenDifficulty");
	}else{
		easy.classList.add("chosenDifficulty");
	}
		hard.classList.remove("correctColor");
		easy.classList.remove("correctColor");
}

function resetHeader(){
	header.classList.add("heade");
	header.classList.remove('correctColor');
}

function populateNewColorArray(){
	while(maxArrLen > 0){
		colorArray.push(Math.floor(Math.random() * 255));
		maxArrLen--;
	}

	maxArrLen = numTiles * 3; //return to orig value
}

function resetTiles(){
	var numTilesTmp = 0;
	colorBlockList.forEach(function(tile){
		tile.classList.remove("correct");
		showObj(tile);
	});
}

function cleanCssChildren(){
	var head = document.getElementsByTagName("head");
	if(head[0].childElementCount > 1){
		while(head[0].childElementCount > 1){
			head[0].removeChild(head[0].lastElementChild)
		}
	}
};