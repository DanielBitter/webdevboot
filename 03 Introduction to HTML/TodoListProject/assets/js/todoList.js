var btnToggle = $("#btnToggle");
var userTxt = $("#input");
var list = $("ul");
var slideDur = 500;

setup();

function setup() {
	userTxt.focus();
	// $("#input").focus();
	btnToggle.on({
		"click": toggleInput
	});
	userTxt.on("keypress", function(event){
		if(event.which == 13){
			addTodoItem();
		}
	});
	// $("#input").height($("#heading").height());
	// $("#input").css("fontSize", $("#heading").css("fontSize"));
}

function addTodoItem(){
	var li = $("<li></li>");
	var spanDel = $("<div class='deleteBtn' style='width:0px'></div");
	var icoDel = $("<i class='far fa-trash-alt'></i>");
	spanDel.append(icoDel);
	spanDel.on("click", deleteTodo);
	var spanText = $("<div class='todoText contentFontStyle'></div>");
	var txt = userTxt.val();
	if(txt.length > 0){
		spanText.text(txt);
		spanText.on({
		    "mouseenter": addStrike,
		    "mouseleave": removeStrike
		});
		spanText.on("click", toggleStrike);
		li.append(spanDel);
		li.append(spanText);
		li.on({
		    "mouseenter": deleteIcoShow,
		    "mouseleave": deleteIcoHide
		});
		list.append(li);
	}
	userTxt.val("");
}

function addStrike(){
	$(this).addClass("strikeTemp");
}

function removeStrike(){
	$(this).removeClass("strikeTemp");
}

function toggleStrike(){
	$(this).toggleClass("strikeDone");
}

function deleteTodo(){
	$(this).closest("li").fadeOut(slideDur,function(){
		$(this).remove();
	});
}

function deleteIcoHide(){
	$(this).find(".deleteBtn").stop().animate({width:'0px'},
		slideDur,
		function(){
			$(this).find("i").css("visibility","hidden");
		});
}

function deleteIcoShow(){
	$(this).find("i").css("visibility","visible");
	$(this).find(".deleteBtn").stop().animate({width:'40px'},slideDur);	
}

function toggleInput(){
	$("#inputDiv").slideToggle(slideDur);
	if($("#inputDiv").height() > 0){
		$("#btnToggle").removeClass("rotate-45");
	}else{
		$("#btnToggle").addClass("rotate-45");
	}
}
