// console.log("Print -10 to 19");
// var num = -10;
// while (num < 20){
// 	console.log(num);
// 	num++;
// }

// console.log("Print evens 10-40");
// num = 10;
// while (num < 41){
// 	console.log(num);
// 	num+=2;
// }

// console.log("Print odds 300-333");
// num = 301;
// while (num < 334){
// 	console.log(num);
// 	num+=2;
// }

// console.log("Print all 5-50 divisible by 5 & 3");
// num = 6;
// while (num < 51){
// 	if(num%5 == 0){
// 		console.log(num);
// 	}
// 	num+=3;
// }

var thereYetStatus = "no";

while(!(thereYetStatus == "yes" 
		|| thereYetStatus == "yeah")){
	thereYetStatus = prompt("Are we there yet?");
}

thereYetStatus = "Yay, we've arrived!";

alert(thereYetStatus);
console.log(thereYetStatus);
