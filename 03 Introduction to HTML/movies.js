var movieList= [
	{
		name: "\"The Avengers\"",
		rating: 4.5,
		seen: true
	},
	{
		name: "\"Fast & Furious\"",
		rating: 5,
		seen: true
	},
	{
		name: "\"Baby Driver\"",
		rating: 4,
		seen: false
	}
];

movieList.forEach(function(item){
	var seen = "";
	if(!item.seen){
		seen = " not";
	}
	var info = "You have" + seen + " seen " + item.name
		+ " - " + item.rating + " stars";

	console.log(info);
});