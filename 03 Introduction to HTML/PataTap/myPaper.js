paper.install(window);
var toolKit;
window.onload = function() {
	console.log("loaded");
	paper.setup('myCanvas');

	var baseRadius = 200;

	toolKit = new Tool();
	toolKit.onKeyDown = function(event){
		createVisual(event.key);
	}

	function createVisual(keycode){
		var maxPoint = new Point(view.size.width, view.size.height);
		var randomPoint = Point.random();
		var generatedPoint = maxPoint * randomPoint;
		var generatedPoint = maxPoint.multiply(randomPoint);
		
	   	var calcRadius = Math.floor(Math.random() * baseRadius);
	   	
	   	var pathCircle = new Path.Circle({
			center: generatedPoint,
			radius: calcRadius,
			fillColor: 'red'
		});

		var letterPercent = (keycode.charCodeAt(0) - 97) / 26;
		var colorPercent = letterPercent * 18;
		pathCircle.fillColor.hue = colorPercent * colorPercent;

		pathCircle.onFrame = function(event) {
				this.scale(0.7);
				this.fillColor.hue += 1;
		}

		setTimeout(function(){ 
			pathCircle.remove();
		}, 3000);
	}

	paper.view.draw();
}
