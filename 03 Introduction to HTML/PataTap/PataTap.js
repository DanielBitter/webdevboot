console.log("good");
var soundsArray = [];
var colorPairsArray = [];
var fileList = ["bubbles.mp3","clay.mp3","confetti.mp3","corona.mp3","dotted-spiral.mp3","flash-1.mp3","flash-2.mp3","flash-3.mp3","glimmer.mp3","moon.mp3","pinwheel.mp3","piston-1.mp3","piston-2.mp3","piston-3.mp3","prism-1.mp3","prism-2.mp3","prism-3.mp3","splits.mp3","squiggle.mp3","strike.mp3","suspension.mp3","timer.mp3","ufo.mp3","veil.mp3","wipe.mp3","zig-zag.mp3"];
// var colorList = ["954432","C28747","C3C14B","67C95E","44A437","37A46A","2F8E89","2B5C82","503295","A74BC3","CD81D5","BAA7E2","7DADD4","5EC9AB","9CDD98","89D279","CAD071","BF9340","BF9340","C9875E","BB4B3E","C28747","C3C14B","67C95E","44A437","37A46A"];

setup();

function setup() {
	document.onkeypress = function (e) {
	    e = e || window.event;
	    triggerEvents(e.which);
	};
	loadFileListToArray();
}

function triggerEvents(keycode){
	playAudio(keycode);
}

function playAudio(keycode){
	console.log("keypressed: "+keycode);
	if(96 < keycode && keycode < 123){
		keycode -= 32; //equalize A=a, Z=z, etc
	}
	if(64 < keycode && keycode < 91){
		keycode -= 65; //align to array indices
		soundsArray[keycode].play();
	}
}

 function loadFileListToArray(){
 	var counter = 0;
 	fileList.forEach(function(song){
 		createAddHowlObjToArray(song, counter);
 		counter++;
 	});
}

function createAddHowlObjToArray(song, index){	
	soundsArray[index] = new Howl({
	  src: ['./sounds/' + song]
	});
}
